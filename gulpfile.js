var gulp = require('gulp');
var sass = require('gulp-sass');
var autoprefixer = require('gulp-autoprefixer');
var browserSync = require('browser-sync');
var htmlImport = require('gulp-html-import');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var babel = require("gulp-babel");
var rename = require("gulp-rename");
var imagemin = require("gulp-imagemin");
var cache = require("gulp-cache");
var cssnano = require('gulp-cssnano');
var del = require('del');
var spritesmith = require('gulp.spritesmith');
var merge = require('merge-stream');

// SASS/SCSS to CSS
gulp.task('sass', function () {
    gulp.src('src/scss/**/*.scss')
        .pipe(sass({outputStyle: 'expanded'}).on('error', sass.logError))
        .pipe(autoprefixer(['last 15 versions', '> 1%', 'ie 8', 'ie 7'], {cascade: true}))
        .pipe(gulp.dest('app/css'))
});

//All JavaScript libs to library.min.js
gulp.task('library', function () {
    gulp.src('src/library-js/**/*.js')
        .pipe(concat('library.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('app/js'));
});

//Browser server
gulp.task('browser-sync', function () {
    browserSync({
        server: {
            baseDir: 'app'
        },
        notify: false
    });
});

//Main JavaScript babel + uglify
gulp.task('js', function () {
    return gulp.src('src/js/app.js')
        // .pipe(rename({suffix: '.min'}))
        .pipe(babel({presets: ['es2015']}))
        // .pipe(uglify())
        .pipe(gulp.dest('app/js'));
});

//Import HTML components
gulp.task('import', function () {
    gulp.src('src/pages/**/*.html')
        .pipe(htmlImport('src/components/'))
        .pipe(gulp.dest('app'));
});

// Compress IMG
gulp.task('compress', function() {
    del('app/img/*');
    return gulp.src('src/img/**/*')
        .pipe(cache(imagemin({
            interlaced: true,
            progressive: true,
            optimizationLevel: 5,
            svgoPlugins: [{removeViewBox: false}]
        })))
        .pipe(gulp.dest('app/img'));
});

// Create Sprite
gulp.task('sprite', ['css-libs'], function () {
    var spriteData = gulp.src('src/sprite/*.png').pipe(spritesmith({
        imgName: 'sprite.png',
        cssName: 'sprite.css'
    }));

    var imgStream = spriteData.img
        .pipe(gulp.dest('app/css/'));

    var cssStream = spriteData.css
        .pipe(gulp.dest('src/library-css/'));

    return merge(imgStream, cssStream);
});

//All CSS libs to library.min.css
gulp.task('css-libs', function() {
    return gulp.src('src/library-css/**/*.css')
        .pipe(concat('library.css'))
        .pipe(cssnano())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('app/css'));
});

// All Watch
gulp.task('watch', ['browser-sync', 'sass', 'import', 'library', 'js', 'compress', 'css-libs', 'sprite'], function () {
    gulp.watch('src/scss/**/*.scss', ['sass']);
    gulp.watch('src/scss/**/*.scss', browserSync.reload);
    gulp.watch('src/**/*.html', ['import']);
    gulp.watch('src/**/*.html', browserSync.reload);
    gulp.watch('src/library-js/**/*.js', ['library']);
    gulp.watch('src/library-js/**/*.js', browserSync.reload);
    gulp.watch('src/js/app.js', ['js']);
    gulp.watch('src/js/app.js', browserSync.reload);
    gulp.watch('src/img/*', ['compress']);
    gulp.watch('src/img/*', browserSync.reload);
    gulp.watch('src/library-css/**/*.css', ['css-libs']);
    gulp.watch('src/library-css/**/*.css', browserSync.reload);
    gulp.watch('src/sprite/*.png', ['sprite']);
    gulp.watch('src/sprite/*.png', browserSync.reload);
});

// Default Start
gulp.task('default', ['watch']);