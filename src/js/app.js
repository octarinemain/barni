$(document).ready(function () {

    // Rules Swiper
    var rulesSwiper = new Swiper('.rules-swiper', {
        slidesPerView: 4,
        spaceBetween: 0,
        simulateTouch: false,
        navigation: {
            nextEl: '.rules .swiper-button-next',
            prevEl: '.rules .swiper-button-prev'
        },
        breakpoints: {
            767: {
                slidesPerView: 2,
                simulateTouch: true,
                spaceBetween: 10
            },
            479: {
                slidesPerView: 1,
                simulateTouch: true,
                spaceBetween: 10
            }
        }
    });

    // Product Swiper
    var rulesSwiper = new Swiper('.product__swiper', {
        slidesPerView: 3,
        spaceBetween: 0,
        navigation: {
            nextEl: '.product .swiper-button-next',
            prevEl: '.product .swiper-button-prev'
        },
        pagination: {
            el: '.product .swiper-pagination',
            dynamicBullets: true,
            clickable: true
        },
        breakpoints: {
            767: {
                slidesPerView: 2
            },
            479: {
                slidesPerView: 1
            }
        }
    });

    // Cookies
    function cookies() {
        let cookies = $('.cookies');
        let data = sessionStorage.getItem('cookie');
        if (data === null) {
            cookies.addClass('js-show');
        }

        $('.cookies .btn').on('click', function (e) {
            e.preventDefault();
            cookies.removeClass('js-show');
            sessionStorage.setItem('cookie', 'ok');
        });
    }

    cookies();

    // Clamping Footer
    function clampingFooter() {
        var footerW = $('.footer').outerHeight();
        $('.height-helper').css('margin-top', '-' + footerW + 'px');
        $('.content-wrap').css('padding-top', footerW + 'px');
    }

    // Slide Rules double hover
    $('.slide-rules.js-hover').hover(function () {
        $('.js-hover').css('transform', 'scale(1.1)');
        $('.slide-rules__title--common').addClass('active');
    }, function () {
        $('.js-hover').css('transform', 'scale(1)');
        $('.slide-rules__title--common').removeClass('active');
    });

    // PopUp
    function popUp() {
        $('.js-popup-button').on('click', function () {
            $('.popup').removeClass('js-popup-show');
            var popupClass = '.' + $(this).attr('data-popupShow');
            $(popupClass).addClass('js-popup-show');
            $('body').addClass('no-scroll');
        });

        closePopup();
    }

    // Close PopUp
    function closePopup() {
        $('.js-close-popup').on('click', function () {
            $('.popup').removeClass('js-popup-show');
            $('body').removeClass('no-scroll');
        });

        $('.popup').on('click', function (e) {
            var div = $(".popup__wrap");
            if (!div.is(e.target) && div.has(e.target).length === 0) {
                $('.popup').removeClass('js-popup-show');
                $('body').removeClass('no-scroll');
            }
        });
    }

    popUp();

    // Input Password
    $('.js-pass').on('click', function () {
        var input = $(this).closest('div').find('input');
        if (input.attr('type') === 'password') {
            input.attr('type', 'text');
        } else {
            input.attr('type', 'password');
        }
    });

    // Select
    $('select').select2({});

    // Tabs
    function tabs() {
        var tabsContent = $('.content-tabs-wrap .content-tabs');

        // Reset
        $('.nav-tabs li').eq(0).addClass('active');
        tabsContent.eq(0).addClass('active');

        // Tabs Main Action
        $('.nav-tabs').on('click', 'li', function () {
            var i = $(this).index();
            $('.nav-tabs li').removeClass('active');
            $(this).addClass('active');
            tabsContent.removeClass('active');
            tabsContent.eq(i).addClass('active');
        });
    }

    tabs();

    // Jquery Validate
    $('form').each(function () {
        $(this).validate({
            ignore: [],
            errorClass: "error",
            validClass: "success",
            errorElement: "div",
            wrapper: "span",
            rules: {
                name: {
                    required: true,
                    normalizer: function (value) {
                        return $.trim(value);
                    }
                },
                phone: {
                    required: true,
                    phone: true
                },
                email: {
                    required: true,
                    email: true,
                    normalizer: function (value) {
                        return $.trim(value);
                    }
                },
                password: {
                    required: true
                },
                personalAgreement: {
                    required: true
                },
                rules: {
                    required: true
                },
                captcha: {
                    required: true
                }
            },
            messages: {
                phone: {
                    phone: "Укажите номер телефона"
                },
                email: {
                    email: "Введите правильный email"
                }
            }
        });

        jQuery.validator.addMethod("phone", function (value, element) {
            return this.optional(element) || /\+7\(\d+\)\d{3}-\d{2}-\d{2}/.test(value);
        });
    });

    // Scroll
    $('.winners__table').scrollbar();

    // Masked Phone
    $("input[type='tel']").mask("+7(999)999-99-99");

    // Burger Menu
    function mobMenu() {
        $('.header__burger').on('click', function () {
            $(this).toggleClass('active');
            $('.nav').toggleClass('active');

            $(document).on('click', function (e) {
                var div = $(".nav, .header__burger");
                if (!div.is(e.target) && div.has(e.target).length === 0) {
                    $('.header__burger').removeClass('active');
                    $('.nav').removeClass('active');
                }
            });
        });
    }

    mobMenu();

    // List Mob
    $(window).on('load resize', function () {
        clampingFooter();
        var w = $(this).outerWidth();
        var footerH = $('.footer').outerHeight();
        if (w <= 767) {
            $('.body-bg__item--1').css('top', 225 + footerH + 'px');
            $('.body-bg__item--2').css('top', 120 + footerH + 'px');
            $('.body-bg__item--3').css('top', 63 + footerH + 'px');
            $('.body-bg__item--4').css('top', 35 + footerH + 'px');
            $('.body-bg__item--5').css('top', 43 + footerH + 'px');
            $('.body-bg__item--6').css('top', 50 + footerH + 'px');
            $('.body-bg__item--7').css('top', 115 + footerH + 'px');
            $('.body-bg__item--8').css('top', 166 + footerH + 'px');
            $('.body-bg__item--9').css('top', 214 + footerH + 'px');
        }
    });

    function parallaxDesk() {
        $(window).bind('mousewheel', function (event) {
            if (event.originalEvent.wheelDelta >= 0) {
                $('.body-bg-desktop').css('transform', function () {
                    return 'translateY(' + ($(window).scrollTop() * (-0.8)) + 'px)';
                });
            }
            else {
                $('.body-bg-desktop').css('transform', function () {
                    return 'translateY(' + ($(window).scrollTop() * (-0.8)) + 'px)';
                });
            }
        });
    }

    parallaxDesk();
});